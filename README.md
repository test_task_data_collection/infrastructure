# infrastructure

This repository contains build scripts and configs for infrastructure systems. With the help of pipeline, docker images are built and stored in the docker registry. This built image can then be easily deployed on any platform without having to worry about configs.
This solution cannot be used for production, but as a quick and visual solution for the test task, it is quite suitable.


If you want to deploy this image to yourself, you can use the following command:
```
docker run -d -p 8123:8123 -p 9000:9000 -v /data/clickhouse/ch-data:/var/lib/clickhouse/ -v /data/clickhouse/ch-logs:/var/log/clickhouse-server/ --restart always --name clickhouse-data-collector registry.gitlab.com/test_task_data_collection/infrastructure:ch
```